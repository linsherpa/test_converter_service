# Test Converter Service

## Requirements:
- installation of docker 
- Python version 3.9 or higher 

## Creation of REST API
- create a virtual environment at a certain directory (example test-folder).
    - Command: python -m venv <path to test-folder>
    - source <path to the test-folder>/bin/activate
- install the required libraries from Requirements.txt in your virtual environment.
    - Command: pip install -r requirements.txt

- Run the API
    - Go iside the test_converter_service folder
    - Command:python rest_api.py


The api will be on localhost:5000/api-docs#

