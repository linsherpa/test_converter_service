#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: ExpressionTool
label: Extract the location/path of a file

requirements:
  InlineJavascriptRequirement: {}

inputs:
  input_file:
    type: File

outputs:
  output:
    type: string
  output2:
    type: string

expression: |
  ${
  var test=inputs.input_file.location.replace('file://','');
  var test1= test.substr(0,test.lastIndexOf("/"));
  return {"output": test, "output2": test1}; }
